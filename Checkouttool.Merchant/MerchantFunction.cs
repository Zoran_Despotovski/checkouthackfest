
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Checkouttool.Shared.Models;

namespace CheckoutHackfest
{
    public static class MerchantFunction
    {
        [FunctionName("Merchant")]
        public static IActionResult GetMerchant(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = "{id}")]HttpRequest req,
            [CosmosDB("Merchants", "Data", Id = "{id}", ConnectionStringSetting = "MerchantsConnectionString")] MerchantData document,
            TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            //string name = req.Query["name"];

            //string requestBody = new StreamReader(req.Body).ReadToEnd();
            //dynamic data = JsonConvert.DeserializeObject(requestBody);
            //name = name ?? data?.name;



            log.Info($"Selected merchant: {document?.name ?? ""}");

            return document?.id != null
                ? (ActionResult)new OkObjectResult(document)
                : new BadRequestObjectResult("Please pass a name on the query string or in the request body");
        }


    }
}
