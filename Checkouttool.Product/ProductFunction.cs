
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Checkouttool.Shared.Models;

namespace Checkouttool.Product
{
    public static class ProductFunction
    {
        [FunctionName("Product")]
        public static IActionResult GetProduct(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = "{id}")]HttpRequest req,
            [CosmosDB("Product", "Data", Id = "{id}", ConnectionStringSetting = "ProductsConnectionString")] ProductData document,
            TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");
            log.Info($"Selected merchant: {document?.name ?? ""}");

            //string name = req.Query["name"];

            //string requestBody = new StreamReader(req.Body).ReadToEnd();
            //dynamic data = JsonConvert.DeserializeObject(requestBody);
            //name = name ?? data?.name;

            return document != null
                ? (ActionResult)new OkObjectResult(document)
                : new BadRequestObjectResult("Please pass a name on the query string or in the request body");
        }
    }
}
