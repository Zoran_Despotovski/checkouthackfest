﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Checkouttool.Shared.Models
{
    public class CreditCardTokenQueueMessage
    {
        public string consumerid { get; set; }
        public string token { get; set; }
    }
}
