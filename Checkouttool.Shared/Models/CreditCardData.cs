﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Checkouttool.Shared.Models
{
    public class CreditCardData
    {
        public string id { get; set; }
        public string name { get; set; }
        public string accountnumber { get; set; }
        public string validthru { get; set; }
    }
}
