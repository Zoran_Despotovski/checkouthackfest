﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Checkouttool.Shared.Models
{
    public class MerchantData
    {
        public string id { get; set; }
        public string name { get; set; }
        public string logo { get; set; }
        public string[] products { get; set; }
    }
}
