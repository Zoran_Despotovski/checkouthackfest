﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Checkouttool.Shared.Models
{
    public class ProductData
    {
        public string id { get; set; }
        public string name { get; set; }
        public string logo { get; set; }
    }
}
