﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Checkouttool.Shared.Models
{
    public class MerchantWithProducts
    {
        public MerchantData merchant { get; set; }
        public List<ProductData> products { get; set; }

        public MerchantWithProducts()
        {
            products = new List<ProductData>();
        }
    }
}
