using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Checkouttool.Shared.Models;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;

namespace Checkouttool.Orchestrator
{
    public static class OrchestratorFunction
    {
        [FunctionName("OrchestratorFunction")]
        public static async Task<Uri> RunOrchestrator(
            [OrchestrationTrigger] DurableOrchestrationContext context)
        {
            var merchantId = context.GetInput<string>();
            //act1 chained
            var merchantData = await context.CallActivityAsync<MerchantData>("MerchantCap", merchantId);

            var tasks = new Task<ProductData>[merchantData.products.Length];
            for (var i = 0; i < merchantData.products.Length; i++)
            {
                tasks[i] = context.CallActivityAsync<ProductData>("ProductCap", merchantData.products[i]);
            }
            //act2 fan-in/out
            await Task.WhenAll(tasks);

            var merchantWithProducts = new MerchantWithProducts();
            merchantWithProducts.merchant = merchantData;
            foreach (var task in tasks)
            {
                merchantWithProducts.products.Add(task.Result);
            }
            var resultBlobURL = await context.CallActivityAsync<Uri>("StorageActivityFunction", merchantWithProducts);


            //return merchantWithProducts;
            return resultBlobURL;
        }

        [FunctionName("OrchestratorFunction_HttpStart")]
        public static async Task<HttpResponseMessage> HttpStart(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = "merchant/{id}")]HttpRequestMessage req,
            [OrchestrationClient]DurableOrchestrationClient starter,
            string id,
            TraceWriter log)
        {
            // Function input comes from the request content.
            string instanceId = await starter.StartNewAsync("OrchestratorFunction", id);

            log.Info($"Started orchestration with ID = '{instanceId}'.");

            return starter.CreateCheckStatusResponse(req, instanceId);
        }


        [FunctionName("GET_LazuyMerchant")]
        public static async Task<Object> GetMerchant(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = "merchant2/{id}")]HttpRequestMessage req,
            [OrchestrationClient]DurableOrchestrationClient orchestratorClient,
            string id,
            TraceWriter log)
        {
            // Function input comes from the request content.
            string instanceId = await orchestratorClient.StartNewAsync("OrchestratorFunction", id);

            log.Info($"Started orchestration with ID = '{instanceId}'.");

            //if(starter.TaskDuration > 1) return "default result"; else full result

            //return starter.CreateCheckStatusResponse(req, instanceId);//.GetFullData


            var resultMerchant = new MerchantData();
            resultMerchant.id = id;

            //var stringContent = orchestratorClient.CreateCheckStatusResponse(req, instanceId);
            //var resultat = JsonConvert.SerializeObject(stringContent.Content.Co).;

            var result = resultMerchant;

            return result;
        }
    }
}