
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using System.Linq;
using Checkouttool.Shared.Models;
using Checkouttool.Shared;

namespace Checkouttool.Orchestrator
{
    public static class ProductCapFunction
    {
        [FunctionName("ProductCap")]
        public async static Task<ProductData> GetProductAync([ActivityTrigger] string id)
        {
            return await Helpers.GetProduct(id);
        }
    }
}
