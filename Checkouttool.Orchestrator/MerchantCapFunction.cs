
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Linq;
using System.Net.Http;
using Checkouttool.Shared.Models;

namespace Checkouttool.Orchestrator
{
    public static class MerchantCapFunction
    {
        private static HttpClient httpClient = new HttpClient();

        [FunctionName("MerchantCap")]
        public async static Task<MerchantData> GetMerchantAync([ActivityTrigger] string id)
        {
            var functionUrl = System.Environment.GetEnvironmentVariable("MerchantFunctionUrl");
            var merchantId = id;
            var result = await httpClient.GetStringAsync($"{functionUrl}/{merchantId}");

            var resultAsObject = JsonConvert.DeserializeObject<MerchantData>(result);

            return resultAsObject;
        }
    }
}
