
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using System;
using Checkouttool.Shared.Models;
using Checkouttool.Shared;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Threading.Tasks;

namespace Checkouttool.Orchestrator
{
    public static class StorageActivityFunction
    {
        [FunctionName("StorageActivityFunction")]
        public async static Task<Uri> GetBlobUri([ActivityTrigger] MerchantWithProducts merchantWithProducts)
        {
            return await Helpers.SaveResultToBlob(merchantWithProducts);
        }
    }
}
