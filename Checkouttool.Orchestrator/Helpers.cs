﻿using Checkouttool.Shared.Models;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Checkouttool.Orchestrator
{
    public static class Helpers
    {
        private static HttpClient httpClient = new HttpClient();

        public static async Task<Uri> SaveResultToBlob(MerchantWithProducts merchantWithProducts)
        {
            // Load the connection string for use with the application. The storage connection string is stored
            // in an environment variable on the machine running the application called storageconnectionstring.
            // If the environment variable is created after the application is launched in a console or with Visual
            // Studio, the shell needs to be closed and reloaded to take the environment variable into account.
            string storageConnectionString = Environment.GetEnvironmentVariable("StorageConnectionString");
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storageConnectionString);

            // Create the CloudBlobClient that is used to call the Blob Service for that storage account.
            CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

            // Create a container called 'merchantwithproducts'. 
            var cloudBlobContainer = cloudBlobClient.GetContainerReference("merchantwithproducts");
            await cloudBlobContainer.CreateIfNotExistsAsync();

            var cloudBlockBlobReference = cloudBlobContainer.GetBlockBlobReference($"{merchantWithProducts.merchant.id}.json");
            cloudBlockBlobReference.Properties.ContentType = "application/json";
            //await cloudBlockBlobReference.SetPropertiesAsync();

            var objectAsJson = JsonConvert.SerializeObject(merchantWithProducts);


            using (var stream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(stream))
                {
                    streamWriter.Write(objectAsJson);
                    streamWriter.Flush();
                    stream.Position = 0;

                    await cloudBlockBlobReference.UploadFromStreamAsync(stream);
                }
            }

            var resultUrl = cloudBlockBlobReference.Uri;

            return resultUrl;
        }

        public static async Task<ProductData> GetProduct(string productId)
        {
            var functionUrl = System.Environment.GetEnvironmentVariable("ProductFunctionUrl");
            var result = await httpClient.GetStringAsync($"{functionUrl}/{productId}");

            var resultAsObject = JsonConvert.DeserializeObject<ProductData>(result);

            return resultAsObject;
        }

        public static async Task<List<ProductData>> GetProducts(string[] productIds)
        {
            var products = new List<ProductData>();
            foreach (var id in productIds)
            {
                products.Add(await GetProduct(id));
            }

            return products;
        }

        public static async Task<MerchantWithProducts> CreateMerchantWithProducts(MerchantData merchant)
        {
            var merchantWithProducts = new MerchantWithProducts();
            merchantWithProducts.merchant = merchant;
            merchantWithProducts.products = await GetProducts(merchant.products);

            return merchantWithProducts;
        }

        //public static async Task<MerchantWithProducts> GetMerchantWithProductsAsync(string merchantId)
        //{

        //}
    }
}
