
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Checkouttool.Shared.Models;

namespace Checkout.Token
{
    public static class TokenFunction
    {
        [FunctionName("TokenFunction")]
        public static IActionResult Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "create")]HttpRequest req,
            [Queue(queueName: "checkouttool-consumer-creditcard", Connection = "StorageQueueConnectionstring")]IAsyncCollector<CreditCardTokenQueueMessage> queue,
            TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            string requestBody = new StreamReader(req.Body).ReadToEnd();
            var data = JsonConvert.DeserializeObject<CreditCardData>(requestBody);

            var queueMessage = new CreditCardTokenQueueMessage()
            {
                consumerid = data.id,
                token = data.GetHashCode().ToString()
            };
            queue.AddAsync(queueMessage);


            return queueMessage != null
                ? (ActionResult)new OkObjectResult(queueMessage)
                : new BadRequestObjectResult("Please post credit card details in the request body");
        }
    }
}
